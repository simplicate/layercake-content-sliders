<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class SlidersController extends LayerCakeAppController {

	var $name       = 'Sliders';
    var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'Xml' );
    var $components = array( 'Session', 'Auth', 'Email', 'Content.PageContent' );
    var $uses       = array( 'Sliders.Slider' );

    function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'view', 'index' );
	}

    // admin index
    function admin_index() {

        $this->paginate = array( );

		$this->Slider->recursive = 1;
		$this->set( 'sliders', $this->paginate( 'Slider' ) );
	}


	// admin - add a new record
    function admin_add() {

		// where to go after saving
        if( strstr( $this->referer(), '/sliders/index' ) || $this->referer() == '/admin/sliders/' || $this->referer() == '/admin/sliders' ) {
			$this->Session->write( "History.Slider.Add", $this->referer() );
		}

        // save record
        $this->admin_save();

        // output form
        $this->admin_form();
	}


	// admin - edit an existing record
    function admin_edit( $id = null ) {
		if( !$id && empty($this->data) ) {
			$this->Session->setFlash(__('Invalid Record', true), 'default', array( 'class' => 'error' ) );
			$this->redirect( $this->referer() );
		}

		// where to go after saving
        if( strstr( $this->referer(), '/sliders/index' ) || $this->referer() == '/admin/sliders/' || $this->referer() == '/admin/sliders' ) {
			$this->Session->write( "History.Slider.Edit." . $id, $this->referer() );
		}

        // save record
        $this->admin_save();

        // output form
        $this->admin_form( $id );
	}


    // admin - output form
    private function admin_form( $id = null ) {

        // load record to edit
        if( isset( $id ) & empty($this->data) ) { $this->data = $this->Slider->read( null, $id ); }

        // output form
        $this->render( 'admin_form' );
    }


    // admin - save record
    private function admin_save() {

        // don't bother saving unless there's data to save
        if( !empty($this->data) ) {

            $this->Slider->create();

            // clean html input
            App::import( 'Vendor', 'LayerCake.htmlcleaner' );
            $cleaner = new htmlcleaner;
            $this->data['Slider']['text'] = $cleaner->clean_img_floats(    $this->data['Slider']['text'] );
            $this->data['Slider']['text'] = $cleaner->clean_inline_styles( $this->data['Slider']['text'] );

            // try to save the record
            if( $this->Slider->save( $this->data ) ) {

                // redirect to success page
                $this->Session->setFlash(__('The Record has been saved', true), 'default', array( 'class' => 'success' ) );
                $history  = ( $this->data['Slider']['id'] ) ? $this->Session->read( "History.Slider.Edit." . $this->data['Slider']['id'] ) : $this->Session->read( "History.Slider.Add" );
                $this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );

            // bad save
            } else {
                $this->Session->setFlash(__('The Record could not be saved. Please, try again.', true), 'default', array( 'class' => 'error' ) );
            }
		}
    }


	// admin - delete record
    function admin_delete($id = null) {
		// make sure the id has been set before we try to delete
        if( !$id ) {
			$this->Session->setFlash(__('Invalid ID for Record', true), 'default', array( 'class' => 'error' ) );
			$this->redirect( $this->referer() );
		}

		// do the delete
        if ($this->Slider->delete($id)) {

            // redirect to success page
			$this->Session->setFlash(__('Record deleted', true), 'default', array( 'class' => 'success' ) );
			$this->redirect( $this->referer() );
		}
    }
}