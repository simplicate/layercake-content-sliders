<h1>Sliders</h1>

<?
    $paginate_opts           = $this->passedArgs;
    $paginate_opts['plugin'] = NULL;
    $this->Paginator->options( array( 'url' => $paginate_opts ) );
?>

<div class="form-left">
    <?= $this->Html->link( "New Slider", '/admin/sliders/add', array( 'class' => 'button', 'escape' => false )  ); ?>
</div>

<div class="form-right" style="text-align:right;">

</div>
<div class="clear"></div>

<? if( sizeof( $sliders ) >= 1 ): ?>
    <table class="list">
        <thead>
            <tr>
                <th><?= $paginator->sort('name');?></th>
                <th><?= $paginator->sort('location');?></th>
                <th><?= $paginator->sort('text');?></th>
                <th><?= $paginator->sort('status');?></th>
                <th><?= $paginator->sort('ordering');?></th>
                <th class="t-center">Actions</th>
            </tr>
        </thead>

        <tbody>
            <? foreach( $sliders AS $slider ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $slider['Slider']['name']; ?></td>
                    <td><?= $slider['Slider']['location']; ?></td>
                    <td><?= $slider['Slider']['text']; ?></td>
                    <td><?= $slider['Slider']['status']; ?></td>
                    <td><?= $slider['Slider']['ordering']; ?></td>

                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"       />', array( 'action' => 'edit',      $slider['Slider']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete"     />', array( 'action' => 'delete',    $slider['Slider']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $slider['Slider']['name']) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <? if( $this->params['paging']['Slider']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? else: ?>

    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>

<? endif; ?>