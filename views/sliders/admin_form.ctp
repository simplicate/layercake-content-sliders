<h1>Edit Slider</h1>

<?= $form->create( 'Slider', array( 'id' => 'edit-form', 'type' => 'file' ) );?>

<?= $form->input( 'id' ); ?>

<div class="form-left">
    <?= $form->input( 'name',           array( 'class' => 'required' ) ); ?>

    <?= $form->input( 'cta_link_text',  array( 'class' => 'required' ) ); ?>

    <?= $form->input( 'cta_link_url',   array( 'class' => 'required' ) ); ?>

    <?= $form->input( 'text',           array( 'class' => 'ck-simple', 'wrap' => 'off' ) ); ?>
</div>

<div class="form-right">

    <?= $form->input( 'location',       array( 'type' => 'select', 'options' => array( 'home' => 'home' ) ) ); ?>

    <?= $form->input( 'ordering',       array( 'class' => 'required' ) ); ?>

    <?= $form->input( 'status',         array( 'type' => 'select', 'options' => array( 'draft' => 'Draft', 'published' => 'Published' ) ) ); ?>

    <?
        $image_url = "";
        if( !empty( $this->data['Slider']['image_url'] ) ) {
            $image_url = '<img src="' . $this->data['Slider']['image_url'] . '" width="462" />';
        }
    ?>
    <?= $form->input( 'image_url',      array( 'label' => 'Associated Image', 'class' => 'required', 'style' => 'width:70%;', 'between' => $image_url, 'after' => '&nbsp;<input type="button" value="Browse Server" onclick="BrowseServer( \'Images:/sliders\', \'SliderImageUrl\' );" />' ) ); ?>

    <div class="clear"></div>
</div>

<div class="clear"></div>

<br><br>
<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link',  array( "HistoryModel" => "Slider" ) ); ?>
<?= $this->element( 'ck_editor',    array( "plugin" => "admin", "type" => array( "regular", "simple" ) ) ); ?>
<?= $this->element( 'ck_finder' ); ?>