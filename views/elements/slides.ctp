<?php
    // import required model
    App::Import( 'Model', 'Sliders.Slider' );
    $Slider  = new Slider();

    // make sure we're only finding published blog
    $find_conditions = array(
        "Slider.status"   => "published",
        "Slider.location" => $location,
    );

    // find the cached version of this blog query
    $cache_key      = md5( serialize( array( $find_conditions, $location ) ) );
    $slider_cache   = Cache::read( 'slider_cache' , 'default' );
    $slider_cache   = ( is_array( $slider_cache ) ) ? $slider_cache : array();
    $slides         = ( isset( $slider_cache[$cache_key] ) ) ? $slider_cache[$cache_key] : array();

    // find all sliders
    if( !count( $slides ) ) {
        $slides = $Slider->find( 'all', array( 'conditions' => $find_conditions, 'order' => array( 'ordering' ) ) );

        // do a little touching up of all the slides
        foreach( $slides AS &$slide ) {

        }

        // write to cache
        $slider_cache[$cache_key] = $slides;
        Cache::write( 'slider_cache', $slider_cache, 'default' );
    }

    // return inside a layout
    echo $this->element( $layout, array( "slides" => $slides ) );
?>