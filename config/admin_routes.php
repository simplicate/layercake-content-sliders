<?
    Router::connect( '/admin/sliders',                     array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'sliders', 'controller' => 'sliders', 'action' => 'index' ) );
    Router::connect( '/admin/sliders/:action/*',           array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'sliders', 'controller' => 'sliders' ) );
?>